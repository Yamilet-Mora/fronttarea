import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})

export class FrontapiService {
  public URL = 'http://127.0.0.1:3900'
  public url: any;
  constructor(private _http:HttpClient) {
    this.url = this.URL;
  }

  public getDatosProd(){
    const url=`${this.url}/obtener/producto`
    return this._http.get(url);
  }

  public getDatosEmpl(){
    const url=`${this.url}/obtener/empleado`
    return this._http.get(url);
  }

  public getDatosProv(){
    const url=`${this.url}/obtener/proveedor`
    return this._http.get(url);
  }

  public getDatosSucur(){
    const url=`${this.url}/obtener/sucursal`
    return this._http.get(url);
  }

}
