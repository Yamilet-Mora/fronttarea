import { Component, OnInit } from '@angular/core';
import { FrontapiService } from './services/service/frontapi.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  datosProd: any[] = [];
  datosEmpl: any[] = [];
  datosProv: any[] = [];
  datosSucur: any[] = [];
  constructor (private _service:FrontapiService){}

  ngOnInit(): void {
    this.consulta1();
    this.consulta2();
    this.consulta3();
    this.consulta4();
  }

  consulta1(){
    this._service.getDatosProd().subscribe((resp:any) => {
      console.log(resp.respuesta);
      this.datosProd = resp.respuesta;
    });
  }

  consulta2(){
    this._service.getDatosEmpl().subscribe((resp:any) => {
      console.log(resp.respuesta);
      this.datosEmpl = resp.respuesta;
    });
  }

  consulta3(){
    this._service.getDatosProv().subscribe((resp:any) => {
      console.log(resp.respuesta);
      this.datosProv = resp.respuesta;
    });
  }

  consulta4(){
    this._service.getDatosSucur().subscribe((resp:any) => {
      console.log(resp.respuesta);
      this.datosSucur = resp.respuesta;
    });
  }

}
