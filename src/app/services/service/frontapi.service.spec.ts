import { TestBed } from '@angular/core/testing';

import { FrontapiService } from './frontapi.service';

describe('FrontapiService', () => {
  let service: FrontapiService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FrontapiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
